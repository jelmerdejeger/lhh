<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link href="opmaak.css" rel="stylesheet" type="text/css"/>
        <link href='https://fonts.googleapis.com/css?family=Macondo' rel='stylesheet' type='text/css'>
        <meta charset="UTF-8">
        <title>Levende Historie Harderwijk</title>

    </head>
    <body>
        <div class="banner">
            <img src="http://www.levendehistorieharderwijk.nl/pics/intro2.gif"/>
        </div>
            
        <div class="nav">
                <div class="navmenu">
                    <ul>
                        <li><a href="Hoofdpagina.php">Hoofdpagina</a></li>
                    </ul>

                    <ul>
                        <li><a href="info.php">Over ons</a></li>
                    </ul>
                    
                    <ul>
                        <li><a href="Agenda.php">Agenda</a></li>
                    </ul>
                    
                    <ul>
                        <li><a href="Gastenboek.php">Gastenboek</a></li>
                    </ul>
                    
                    <ul>
                        <li><a href="Fotoboek.php">Fotoboek</a>                                
                            <ul>
                                <li><a href="">Evenement 1</a></li>
                                <li><a href="">Evenement 2</a></li>
                                <li><a href="">Evenement 3</a></li>
                                <li><a href="">Evenement 4</a></li>
                                <li><a href="">Evenement 5</a></li>
                            </ul>
                        </li>
                    </ul>
                    
                    <ul>
                        <li><a href="Personages.php">Personages</a></li>
                    </ul>
                </div>    
                
            <p class="menu2"><a href="">Inloggen</a></p>
        </div>
        <div class="info">
            <div class="personage">
                <img class="personagefoto" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg">
            </div>
            <div class="personage">
                <img class="personagefoto" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg">
            </div>
            <div class="personage">
                <img class="personagefoto" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg">
            </div>
            <div class="personage">
                <img class="personagefoto" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg">
            </div>
            <div class="personage">
                <img class="personagefoto" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg">
            </div>
            <div class="personage">
                <img class="personagefoto" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg">
            </div>
            <div class="personage">
                <img class="personagefoto" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg">
            </div>
            <div class="personage">
                <img class="personagefoto" src="http://shackmanlab.org/wp-content/uploads/2013/07/person-placeholder.jpg">
            </div>
        </div>
    </body>
</html>
